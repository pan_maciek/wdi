#include <iostream>

int NWD(int a, int b) {
  if (b == 0) return a;
  return NWD(b, a % b);
}

int main() {
  int a, b, c;
  std::cin >> a >> b >> c;
  std::cout << NWD(NWD(a, b), c) << std::endl;
}