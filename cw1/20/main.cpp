#include <cmath>
#include <iostream>

const double eps = 1e-8;

int main() {
  double a, b, tmp;
  std::cin >> a >> b;
  while (abs(a - b) > eps) {
    tmp = sqrt(a * b);
    b = (a + b) / 2.0;
    a = tmp;
  }
  std::cout << a;
  return 0;
}
