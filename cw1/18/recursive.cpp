#include <iostream>
#include <cmath>

const double eps = 1e-8;

double root3(double a, double x1 = 1) {
  double x2 = (2 * x1 + a / (x1 * x1)) / 3.0;
  if (std::abs(x2 - x1) < eps) return x2;
  else return root3(a, x2);
}

int main() {
  double x;
  std::cin >> x;
  std::cout << root3(x) << std::endl;
  return 0;
}