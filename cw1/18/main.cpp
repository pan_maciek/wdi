#include <iostream>

const double eps = 1e-8;

double root3(double a) {
  double x1 = 1, x2 = 0;
  while (std::abs(x2 - x1) > eps) {
    x2 = x1;
    x1 = (2 * x1 + a / (x1 * x1)) / 3.0;
  }
  return x1;
}

int main() {
  double x;
  std::cin >> x;
  std::cout << root3(x) << std::endl;
  return 0;
}