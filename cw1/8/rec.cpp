#include <cmath>
#include <iostream>

const double eps = 1e-8;

double f(double x) { return pow(x, pow(x, x)); }

double find(double x, double a, double b) {
  double c = (a + b) / 2.0;
  if (b - a < eps) return c;
  if (f(c) - x < 0) return find(x, c, b);
  else return find(x, a, c);
}

int main() {
  std::cout << find(2017, 2, 3) << std::endl;
  return 0;
}
