#include <cmath>
#include <iostream>

const double eps = 1e-8;

double f(double x) { return pow(x, pow(x, x)); }

double find(double x, double a, double b) {
  double c;

  while (b - a >= eps) {
    c = (a + b) / 2.0;
    if (f(c) - x < 0) a = c;
    else b = c;
  }
  return (a + b) / 2.0;
}

int main() {
  std::cout << find(2017, 2, 3) << std::endl;
  return 0;
}
