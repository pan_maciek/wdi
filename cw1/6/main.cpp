#include <cmath>
#include <iostream>

bool is_prime(int x) {
  if (x < 2) return false;
  if (x == 2) return true;
  if (x % 2 == 0) return false;
  for (int s = sqrt(x), i = 3; i <= s; i += 2)
    if (x % s == 0) return false;
  return true;
}

int main() {
  int x;
  std::cin >> x;
  std::cout << (is_prime(x) ? "true" : "false") << std::endl;
  return 0;
}