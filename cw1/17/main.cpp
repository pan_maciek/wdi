#include <iostream>
#include <cmath>

const double eps = 1e-8;

int main(int argc, char const *argv[]) {
  double a, b, c;
  double q = 0, prev_q;
  std::cin >> a >> b;
  do {
    prev_q = q;
    q = a / b;
    c = b;
    b = a;
    a = c + b;
  } while (std::abs(q - prev_q) > eps);
  std::cout << q << std::endl;
  return 0;
}
