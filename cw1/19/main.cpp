#include <cmath>
#include <iostream>

const double eps = 1e-8;

int main() {
  double e = 2, e_old = 1, fact = 2, n = 3;
  while (e - e_old > eps) {
    e_old = e;
    e += 1 / fact;
    fact *= n;
    n++;
  }
  std::cout << e << std::endl;
  return 0;
}
