#include <cmath>
#include <iostream>

const double eps = 1e-8;

double pi() {
  double a = sqrt(0.5), prod = a, prod_old = 0;
  while (std::abs(prod - prod_old) > eps) {
    prod_old = prod;
    prod *= a = sqrt(0.5 * (1 + a));
  }
  return 2.0 / prod;
}

int main() {
  std::cout << pi() << std::endl;
  return 0;
}
