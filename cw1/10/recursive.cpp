#include <cmath>
#include <iostream>

const double eps = 1e-8;

double pi(double a = sqrt(0.5), double prod = sqrt(0.5)) {
  if (abs(prod - prod / a) <= eps)
    return 2.0 / prod;
  a = sqrt(0.5 * (1 + a));
  return pi(a, prod * a);
}

int main() {
  std::cout << pi() << std::endl;
  return 0;
}