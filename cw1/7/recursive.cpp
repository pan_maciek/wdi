#include <cmath>
#include <iostream>

const double eps = 1e-8;

double sqrt(double a, double x1 = 1) {
  double x2 = 0.5 * (x1 + a / x1);
  if (abs(x2 - x1) < eps) return x2;
  else return sqrt(a, x2);
}

int main() {
  double x;
  std::cin >> x;
  std::cout << sqrt(x, 1) << std::endl;
  return 0;
}