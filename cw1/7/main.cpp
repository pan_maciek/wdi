#include <cmath>
#include <iostream>

const double eps = 1e-8;

double sqrti(double a) {
  double x1 = 1, x2 = 0;
  while (abs(x2 - x1) > eps) {
    x2 = x1;
    x1 = (x1 + a / x1) / 2.0;
  }
  return x1;
}

int main() {
  double x;
  std::cin >> x;
  x = sqrti(x);
  std::cout << x << std::endl;
  return 0;
}