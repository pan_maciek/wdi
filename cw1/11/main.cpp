#include <cmath>
#include <iostream>

int main(int argc, char const *argv[]) {
  int x, i = 1;
  std::cin >> x;
  double p = sqrt(x);
  while (i < p) {
    if (x % i == 0) {
      std::cout << i << " " << x / i << " " << std::endl;
    }
    i++;
  }
  if (i * i == x)
    std::cout << i << std::endl;
  return 0;
}
