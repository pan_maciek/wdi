#include <cmath>
#include <iostream>

bool is_prerfect(int n) {
  if (n % 2 == 1) return false;
  int sum = n - 1, i = 2;
  double p = sqrt(n);
  while (i < p) {
    if (n % i == 0) {
      sum -= i + n / i;
    }
    i++;
  }
  if (i * i == n) sum -= i;
  return sum == 0;
}

int main() {

  for (int i = 6; i < 1000000; i++) {
    if (is_prerfect(i))
      std::cout << i << " ";
  }
  std::cout << std::endl;
}