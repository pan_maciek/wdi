#include <iostream>

bool is_prime(int x) {
  if (x < 2) return false;
  if (x == 2) return true;
  if (x % 2 == 0) return false;
  int i = 3;
  while (i * i <= x) {
    if (x % i == 0) return false;
    i += 2;
  }
  return true;
}

int main(int argc, char const *argv[]) {
  long long p = 2, q = 1;

  while (q < 1000000) {
    // (2 ^ p) * (2 ^ (p-1) - 1)
    q = (1 << (p - 1)) * ((1 << p) - 1);
    if (is_prime((1 << p) - 1))
      std::cout << q << " ";
    p++;
  }
  return 0;
}
