#include <iostream>

int main() {
  int x;
  std::cin >> x;
  int a = 1, b = 0, c, sum = 0;
  while (sum < x) {
    sum += a;
    c = b;
    b = a;
    a = c + b;
  }
  a = 1;
  b = 0;
  while (sum > x) {
    sum -= a;
    c = b;
    b = a;
    a = c + b;
  }
  std::cout << (sum == x ? "true" : "false") << std::endl;
}