#include <iostream>

int main() {
  int x, a = 1, b = 1, c;
  std::cin >> x;
  while (a * b < x) {
    c = b;
    b = a;
    a = b + c;
  }
  std::cout << (x == a * b ? "true" : "false") << std::endl;
  return 0;
}