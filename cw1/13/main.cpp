#include <cmath>
#include <iostream>

int sumDivs(int a) {
  int s = sqrt(a), sum = 1, i = 2;
  while (i <= s) {
    if (a % i == 0) {
      sum += i;
      if (i * i != a) sum += a / i;
    }
    i++;
  }
  return sum;
}

int main() {
  for (int i = 1, zap; i < 1000000; i++) {
    zap = sumDivs(i);
    if (zap > i && sumDivs(zap) == i) {
      std::cout << i << ", " << zap << std::endl;
    }
  }
  return 0;
}