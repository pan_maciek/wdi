#include <iostream>

int sqrt(int x, int n = 0, int k = 1) {
  if (x < 0) return n - 1;
  else sqrt(x - k, n + 1, k + 2);
}

int main() {
  int x;
  std::cin >> x;
  std::cout << sqrt(x) << std::endl;
  return 0;
}