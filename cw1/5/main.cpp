#include <iostream>

int sqrt(int x) {
  int n = 0, k = 1;
  while (x >= 0) {
    x -= k;
    k += 2;
    n++;
  }
  return n - 1;
}

int main() {
  int x;
  std::cin >> x;
  std::cout << sqrt(x) << std::endl;
  return 0;
}