#include <cmath>
#include <iostream>

const double fi = 1.61803398874989484820;

int main() {
  int a = 1, b, c;
  std::cin >> c;
  b = round(c / fi);
  while(a > 0) {
    a = c - b;
    if (a < 0) break;
    c = b;
    b = a;
  }
  std::cout << b << ' ' << c << std::endl;
  return 0;
}