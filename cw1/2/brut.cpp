#include <iostream>

int is_in_fib(int n, int b, int a) {
  int c;
  while (b < n) {
    c = b;
    b = a;
    a = b + c;
  }
  return b == n;
}

int main() {
  int x, min, min_a, min_b;
  std::cin >> x;
  min = x;
  for (int a = 0; a < x; a++) {
    for (int b = 1; b < x; b++) {
      if (is_in_fib(x, a, b)) {
        if (b + a < min) {
          min_a = a;
          min_b = b;
          min = b + a;
        }
      }
    }
  }
  std::cout << min_a << ' ' << min_b << std::endl;

  return 0;
}