#include <cmath>
#include <iostream>

const double PI = 3.14159265358979323846;
const double eps = 1e-8;

int cos_sign(double x) {
  int sing = (int)(x / PI) % 2;
  if (sing % 2 == 0) return 1;
  else return -1;
}

double reduce_cos_arg(double x) {
  while (x < 2 * PI) x += 2 * PI;
  while (x > 2 * PI) x -= 2 * PI;
  return x;
}

double cos(double x) {
  int sign = cos_sign(x);
  x = reduce_cos_arg(x);
  int n = 2;
  double sum_old = 0, sum = 1, pow = -x * x, fact = 2;
  while (abs(sum_old - sum) > eps) {
    sum_old = sum;
    sum += pow / fact;
    pow *= -x * x;
    fact *= (2 * n) * (2 * n - 1);
    n++;
  }
  return sum;
}

int main() {
  int x;
  std::cin >> x;
  std::cout << cos(x) << std::endl;
  return 0;
}
