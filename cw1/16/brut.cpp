#include <iostream>

int steps(int An) {
  int steps = 0;
  while (An != 1) {
    An = (An % 2) * (3 * An + 1) + (1 - An % 2) * An / 2;
    steps++;
  }
  return steps;
}

int main() {
  int max, max_steps = 0;
  for (int i = 2, s; i <= 10000; i++) {
    s = steps(i);
    if (s > max_steps) {
      max_steps = s;
      max = i;
    }
  }
  std::cout << max << std::endl;
  return 0;
}