#include <iostream>

int NWD(int a, int b) {
  if (b == 0) return a;
  return NWD(b, a % b);
}

int NWW(int a, int b) {
  return a / NWD(a, b) * b;
}

int main() {
  int a, b, c;
  std::cin >> a >> b >> c;
  std::cout << NWW(NWW(a, b), c) << std::endl;
}
