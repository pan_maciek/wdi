#include <iostream>

void print_fib(int a, int b) {
  if (a >= 1000000) return;
  std::cout << a << ' ';
  print_fib(a + b, a);
}

int main() {
  print_fib(1, 0);
  return 0;
}