#include <iostream>

const int offset = sizeof(unsigned long long int) * 4;
const unsigned long long int maskB = (-1l) << offset;
const unsigned long long int maskA = ~(maskB);

int main() {
  unsigned long long int a = 1;
  while ((a & maskA) < 1000000) {
    std::cout << (a & maskA) << ' ';
    a = ((a & maskA) << offset) | ((a & maskA) + ((a & maskB) >> offset));
  }
  std::cout << std::endl;
  return 0;
}