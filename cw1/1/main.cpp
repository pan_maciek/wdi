#include <iostream>

int main() {
  int a = 1, b = 1, c;

  while (b < 1000000) {
    std::cout << b << ' ';
    c = b;
    b = a;
    a = b + c;
  }
  std::cout << std::endl;
  return 0;
}