#include <iostream>

const int MAX = 3;
int t[MAX][MAX] = {{2, 2, 2}, {2, 1, 1}, {2, 4, 1}};

bool fun(int t[MAX][MAX]) {
  for (int y = 0; y < MAX; y++) {
    bool any = false;
    for (int x = 0; x < MAX; x++) {
      int cp = t[y][x];
      while (cp > 0) {
        if (cp % 2 == 0) break;
        cp /= 10;
      }
      if (cp == 0) {
        any = true;
        break;
      }
    }
    if (!any) return false;
  }
  return true;
}

int main() {
  std::cout << (fun(t) ? "true" : "false") << std::endl;
}