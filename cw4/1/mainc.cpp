const int MAX = 5;

void fill(int t[MAX][MAX]) {
  int x = 0, y = 0;
  int len = MAX - 1;
  int n = 1, dir = 1;

  while (len > 0) {
    for (int i = len; i > 0; x += dir) {
      t[y][x] = n++;
      i--;
    }
    for (int i = len; i > 0; y += dir) {
      t[y][x] = n++;
      i--;
    }
    if (dir == -1) {
      len -= 2;
      x++;
      y++;
    }
    dir = -dir;
  }
  if (len == 0) t[y][x] = n;
}

int main() {
  int t[MAX][MAX];
  fill(t);
  return 0;
}