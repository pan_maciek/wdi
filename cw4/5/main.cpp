#include <iostream>

struct RC {
  int row, col;
};

const int MAX = 3;
int t[MAX][MAX] = {
  {1, 6, 5},
  {1, 2, 1},
  {7, 1, 3}};

RC fun(int t[MAX][MAX]) {
  RC rc;
  rc.row = rc.col = 0;
  int sum_rows[MAX];
  int sum_cols[MAX];
  for (int y = 0; y < MAX; y++) {
    sum_rows[y] = 0;
    sum_cols[y] = 0;
    for (int x = 0; x < MAX; x++) {
      sum_rows[y] += t[y][x];
      sum_cols[y] += t[x][y];
    }
  }
  double max = 0;
  for (int y = 0; y < MAX; y++) {
    for (int x = 0; x < MAX; x++) {
      if (sum_cols[x] / (double)sum_rows[y] > max) {
        rc.row = y;
        rc.col = x;
        max = sum_cols[x] / (double)sum_rows[y];
      }
    }
  }
  return rc;
}

int main() {
  RC wk = fun(t);
  std::cout << "[" << wk.row << "][" << wk.col << "]" << std::endl;
}