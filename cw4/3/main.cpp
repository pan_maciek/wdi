#include <iostream>

const int MAX = 3;
int t[MAX][MAX] = {{1, 2, 2}, {2, 1, 1}, {2, 4, 1}};

bool fun(int t[MAX][MAX]) {
  for (int y = 0; y < MAX; y++) {
    bool all = true;
    for (int x = 0; x < MAX; x++) {
      if (t[y][x] == 0) continue;
      int cp = t[y][x];
      while (cp > 0) {
        if (cp % 2 == 0) break;
        cp /= 10;
      }
      if (cp == 0) {
        all = false;
        break;
      }
    }
    if (all) return	true;
  }
  return false;
}

int main() {
  std::cout << (fun(t) ? "true" : "false") << std::endl;
}