#include <climits>
#include <iostream>

struct RC { int row, col; };

const int MAX = 3;
int t[MAX][MAX] = {
  {1, 6, 5},
  {1, 2, 1},
  {6, 1, 3}};

RC fun(int t[MAX][MAX]) {
  int sum_row, sum_col;
  int max_col, min_row;
  int max_sum_col = INT_MIN;
  int min_sum_row = INT_MAX;

  for (int y = 0; y < MAX; y++) {
    sum_row = 0;
    sum_col = 0;
    for (int x = 0; x < MAX; x++) {
      sum_row += t[y][x];
      sum_col += t[x][y];
    }
    if (sum_col > max_sum_col) {
      max_sum_col = sum_col;
      max_col = y;
    }
    if (sum_row < min_sum_row) {
      min_sum_row = sum_row;
      min_row = y;
    }
  }
  RC rc;
  rc.row = min_row;
  rc.col = max_col;
  return rc;
}

int main() {
  RC rc = fun(t);
  std::cout << "[" << rc.row << "][" << rc.col << "]" << std::endl;
}