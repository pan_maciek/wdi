#include <iostream>

const int MAX = 9;
int T[MAX] = {2, 3, 4, 5, 6, 7, 8, 9, 10};

int get_weight(int n) {
  int primes = 0, div = 2;
  while (n > 1) {
    if (n % div == 0) {
      do {
        n /= div;
      } while (n % div == 0);
      primes++;
    }
    div++;
  }
  return primes;
}

bool can_be_split_up(int T[MAX], int i, int sA, int sB, int sC) {
  if (i == MAX) return sA == sB && sB == sC;
  int weight = get_weight(T[i]);
  return (
    can_be_split_up(T, i + 1, sA + weight, sB, sC) ||
    can_be_split_up(T, i + 1, sA, sB + weight, sC) ||
    can_be_split_up(T, i + 1, sA, sB, sC + weight));
}

bool can_be_split_up(int T[MAX]) {
  return can_be_split_up(T, 0, 0, 0, 0);
}

int main() {
  std::cout << (can_be_split_up(T) ? "false" : "true") << std::endl;
  return 0;
}