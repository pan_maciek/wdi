#include <iostream>

const int MAX = 10;
int T[MAX] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

int count_sets(int T[MAX], int i, int mul, int target, int target_len, int len, int set[]) {
  if (target_len == len) {
    if (mul == target) {
      std::cout << "( ";
      for (int i = 0; i < target_len; i++)
        std::cout << set[i] << ' ';
      std::cout << ')' << std::endl;
    }
    return mul == target ? 1 : 0;
  }
  if (i == MAX) return 0;
  return count_sets(T, i + 1, (set[len] = T[i]) * mul, target, target_len, len + 1, set) + count_sets(T, i + 1, mul, target, target_len, len, set);
}

int count_sets(int T[MAX], int target, int target_len) {
  int *tmp = new int[target_len];
  return count_sets(T, 0, 1, target, target_len, 0, tmp);
}

int main() {
  std::cout << count_sets(T, 12, 2) << std::endl;
}