#include <iostream>

const int MAX = 10;
int T[MAX] = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

bool can_be_weighed(int T[MAX], int i, int target) {
	if (target == 0) return true;
	if (i == MAX) return false;

	if (can_be_weighed(T, i + 1, target - T[i])) {
		std::cout << "R(" << T[i] << ") ";
		return true;
	}
	if (can_be_weighed(T, i + 1, target + T[i])) {
		std::cout << "L(" << T[i] << ") ";
		return true;
	}
	return can_be_weighed(T, i + 1, target);
}

void print_used(int T[MAX], int target) {
	can_be_weighed(T, 0, target);
}

int main() {
	print_used(T, 13);
	return 0;
}
