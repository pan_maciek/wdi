#include <iostream>

const int MAX = 10;
int T[MAX] = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

bool can_be_weighed(int T[MAX], int i, int target, int used[MAX]) {
	if (target == 0) return true;
	if (i == MAX) return false;

	if (can_be_weighed(T, i + 1, target - T[i], used)) {
		used[i] = -1;
		return true;
	}
	if (can_be_weighed(T, i + 1, target + T[i], used)) {
		used[i] = 1;
		return true;
	}
	return can_be_weighed(T, i + 1, target, used);
}

void print_used(int T[MAX], int target) {
	int tmp[MAX];
	for (int i = 0; i < MAX; i++)
		tmp[i] = 0;
	can_be_weighed(T, 0, target, tmp);
	std::cout << "L = { ";
	for (int i = 0; i < MAX; i++) {
		if (tmp[i] == -1) std::cout << '(' << T[i] << ") ";
	}
	std::cout << "}" << std::endl;
	std::cout << "R = { ";
	for (int i = 0; i < MAX; i++) {
		if (tmp[i] == 1) std::cout << '(' << T[i] << ") ";
	}
	std::cout << "} + (" << target << ')' << std::endl;
}

int main() {
	print_used(T, 21);
	return 0;
}
