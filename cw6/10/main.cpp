#include <iostream>

const int MAX = 10;
int T[MAX] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

int count_sets(int T[MAX], int i, int mul, int target, int target_len, int len) {
	if (target_len == len) return mul == target ? 1 : 0;
	if (i == MAX) return 0;
	return count_sets(T, i + 1, mul * T[i], target, target_len, len + 1) + count_sets(T, i + 1, mul, target, target_len, len);
}

int count_sets(int T[MAX], int target, int target_len) {
	return count_sets(T, 0, 1, target, target_len, 0);
}

int main() {
	std::cout << count_sets(T, 12, 3) << std::endl;
}