#include <iostream>

const int MAX = 9;
int T[MAX] = { 2, 3, 4, 5, 6, 7, 8, 9, 10 };

bool can_be_weighed(int T[MAX], int i, int target) {
	if (target < 0) return false;
	if (i == MAX) return false;
	if (target == 0) return true;
	return can_be_weighed(T, i + 1, target - T[i]) || can_be_weighed(T, i + 1, target);
}

bool can_be_weighed(int T[MAX], int target) {
	return can_be_weighed(T, 0, target);
}

int main() {
	std::cout << (can_be_weighed(T, 30) ? "true" : "false") << std::endl;
	return 0;
}
