#include <iostream>

const int MAX = 4;
int T[MAX] = { 1, 4, 5, 11 };

bool can_be_weighed(int T[MAX], int i, int target) {
	if (target == 0) return true;
	if (i == MAX) return false;
	return (
		can_be_weighed(T, i + 1, target - T[i]) ||
		can_be_weighed(T, i + 1, target + T[i]) ||
		can_be_weighed(T, i + 1, target));
}

bool can_be_weighed(int T[MAX], int target) {
	return can_be_weighed(T, 0, target);
}

int main() {
	std::cout << (can_be_weighed(T, 13) ? "true" : "false") << std::endl;
	return 0;
}
