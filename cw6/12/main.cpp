#include <iostream>

void split(int n) {
  if (n == 1) {
    std::cout << " 1";
    return;
  }
  for (int i = n - 1; i > n - i; i--) {
    std::cout << i << "+";
    split(n);
  }
}

int main() {
  split(5);
}