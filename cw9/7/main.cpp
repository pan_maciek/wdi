#include <iostream>
#include <string>

struct Node { std::string val; Node* next; };

bool set_insert(Node *&root, std::string val) {
	Node *p = root, *q = NULL;
	while (p != NULL && p->val < val) {
		q = p;
		p = p->next;
	}
	if (p != NULL && p->val == val) return false;
	Node *tmp = new Node;
	tmp->val = val;
	tmp->next = p;
	if (q == NULL) root = tmp;
	else q->next = tmp;
	return true;
}

int main() {
	Node* root = NULL;
	set_insert(root, "alarm");
	set_insert(root, "arbuz");
	set_insert(root, "budun");
	set_insert(root, "agrest");
}