#include <iostream>

struct node { int val; node *next; };

void init(node*& root) { root = NULL; }

void push_back(node*& root, int val) {
	if (root == NULL) {
		root = new node;
		root->next = NULL;
		root->val = val;
	}
	else push_back(root->next, val);
}

void add1(node* root) {
	if (root->val == 9) {
		root->val = 0;
		add1(root->next);
	} else root->val += 1;
}

int main() {
	node *root;
	init(root);
	push_back(root, 9);
	push_back(root, 9);
	push_back(root, 1);
	// root 9->9->1 liczba 199
	add1(root);
	add1(root);
  // root 1 -> 0 -> 2 liczba 201
}