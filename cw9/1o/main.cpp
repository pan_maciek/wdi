#include <iostream>

struct Node { int val; Node *next; };

bool set_includes(Node *root, int val) {
  return root != NULL && (root->val == val || (root->val < val && set_includes(root->next, val)));
}

void set_insert(Node *&root, int val) {
  Node *p = root, *q = NULL;
  while (p != NULL && p->val < val) {
    q = p;
    p = p->next;
  }
  if (p != NULL && p->val == val) return;
  Node *tmp = new Node;
  tmp->val = val;
  tmp->next = p;
  if (q == NULL) root = tmp;
  else q->next = tmp;
}

void init(Node *&root) {
  root = NULL;
}

int main(int argc, char const *argv[]) {
  Node *p;
  init(p);
  set_insert(p, 100);
  set_insert(p, 121);
  std::cout << set_includes(p, 121);
  return 0;
}
