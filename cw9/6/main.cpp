#include <iostream>

struct node { int val; node *next; };

void init(node*& root) { root = NULL; }

void toggle(node*& root, int val) {
	node *p = root, *q = NULL;
	while (p != NULL && p->val < val) {
		q = p;
		p = p->next;
	}
	if (p != NULL && p->val == val) {
		if (q == NULL) root = p->next;
		else q->next = p->next;
		delete p;
	} else {
		node *tmp = new node;
		tmp->val = val;
		tmp->next = p;
		if (q == NULL) root = tmp;
		else q->next = tmp;
	}
}

int main() {
	node *l,*r, *y;
	init(l);
	
	toggle(l, 1);
	toggle(l, 2);
	toggle(l, 3);
	toggle(l, 4);
  toggle(l, 1);
}