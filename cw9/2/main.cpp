#include <iostream>

struct node { int val; node *next; };

void pop_back(node *&root) {
	if (root == NULL) return;
	if (root->next == NULL) {
		delete root;
		root = NULL;
	} else pop_back(root->next);
}

void init(node *&root) {
  root = NULL;
}

int main() {
  node *p;
  init(p);
  pop_back(p);
}