#include <iostream>

struct node { int val; node *next; };

void push_back(node *&root, int val) {
  if (root == NULL) {
    root = new node;
    root->val = val;
    root->next = NULL;
  } else push_back(root->next, val);
}

void init(node *&root) {
  root = NULL;
}

int main() {
  node *p;
  init(p);
  push_back(p, 65);
  push_back(p, 5);
  std::cout << p->next->val;
}