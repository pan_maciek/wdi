#include <iostream>

struct node { int val; node *next; };

void init(node*& root) { root = NULL; }

void push_back(node*& root, int val) {
	if (root == NULL) {
		root = new node;
		root->next = NULL;
		root->val = val;
	}
	else push_back(root->next, val);
}

void add(node* root1, node* root2, node*& out, int carry) {
	if (root1 == NULL && root2 == NULL && carry == 0) out = NULL;
	else {
		int val = (root1 != NULL ? root1->val : 0) + (root2 != NULL ? root2->val : 0) + carry;
		out = new node;
		out->val = val % 10;
		add(root1 == NULL ? NULL : root1->next, root2 == NULL ? NULL : root2->next, out->next, val / 10);
	}
}

node* add(node* root1, node* root2) {
	node* out = NULL;
	add(root1, root2, out, 0);
	return out;
}

int main() {
	node *l,*r, *y;
	init(l);
	init(r);
	
	push_back(l, 6);
	push_back(l, 7);
	push_back(l, 8);

	push_back(r, 7);
	push_back(r, 7);
	push_back(r, 7);

	y = add(l, r);
}