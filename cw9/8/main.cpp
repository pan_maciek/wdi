#include <iostream>

struct node { int val; node *next; };

void init(node*& root) { root = NULL; }

void reverse(node* root) {
	node* queue_first = NULL, *tmp, *root_cp = root;
	while (root != NULL) {
		tmp = new node;
		tmp->val = root->val;
		tmp->next = queue_first;
		queue_first = tmp;
		root = root->next;
	}
	root = root_cp;
	while (queue_first != NULL) {
		root->val = queue_first->val;
		root = root->next;
		root_cp = queue_first->next;
		delete queue_first;
		queue_first = root_cp;
	}
}

int main() {
	node *l = new node;
	node * p = l;
	for (int i = 0; i < 4000000; i++)
	{
		p->val = i;
		p = (p->next = new node);
		
	}
	p->val = 4000000;
	p->next = NULL;

	reverse(l);
}