#include <iostream>

struct Node {
  int index, val;
  Node *next;
};

int value(Node *root, int n) {
  return (root != NULL && root->index <= n) ? (root->index == n ? root->val : value(root->next, n)) : 0;
}

void set(Node *&root, int n, int val) {
  Node *p = root, *q = NULL;
  while (p != NULL && p->index < n) {
    q = p;
    p = p->next;
  }
  if (q != NULL && q->index == n) {
    q->val = val;
    return;
  }
  Node *tmp = new Node;
  tmp->val = val;
  tmp->index = n;
  tmp->next = p;
  if (q == NULL) root = tmp;
  else q->next = tmp;
}

void init(Node *&root) {
  root = NULL;
}

int main(int argc, char const *argv[]) {
  Node *p;
  init(p);
  set(p, 10, 100);
  set(p, 11, 121);
  std::cout << value(p, 11);
  return 0;
}
