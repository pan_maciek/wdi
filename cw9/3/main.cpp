#include <iostream>

struct node { int val; node *next; };

void pop_2n(node* root) {
	if (root != NULL && root->next != NULL) {
		node* p = root->next;
		root->next = p->next;
		delete p;
		pop_2n(root->next);
	}
}

void init(node *&root) {
  root = NULL;
}

int main() {
  node *p;
  init(p);
  pop_2n(p);
}