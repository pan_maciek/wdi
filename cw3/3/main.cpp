#include <iostream>

int main() {
  int max;
  std::cin >> max;
  bool *sieve = new bool[max];
  for (int i = 2; i < max; i++) {
    sieve[i] = true;
  }
  sieve[0] = sieve[1] = false;
  for (int i = 2; i < max; i++) {
    if (sieve[i]) {
      std::cout << i << ' ';
      for (int j = i + i; j < max; j += i)
        sieve[j] = false;
    }
  }
  std::cout << std::endl;
  return 0;
}