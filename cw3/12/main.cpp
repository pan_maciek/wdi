#include <iostream>

const int N = 16;
const int t[N] = {1, 2, 4, 8, 24, 72, 216, 648, 2, 3, 44, 5, 6, 71, 8, 9};

int main() {
  int max_len = 1;
  double q;
  for (int i = 1, j; i < N; i++) {
    q = (double)t[i] / t[i - 1];
    for (j = i; j < N && (double)t[j] / t[j - 1] == q;) j++;
    if (j - i + 1 > max_len) max_len = j - i + 1;
  }
  std::cout << max_len << std::endl;
  return 0;
}