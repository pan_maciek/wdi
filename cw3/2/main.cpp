#include <iostream>

const int MAX = 10;
int main() {
  int digits[MAX] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  int a, b;
  std::cin >> a >> b;
  while(a > 0 && b > 0) {
    digits[a % 10]++;
    digits[b % 10]--;
    a /= 10;
    b /= 10;
  }
  if (a != 0 || b != 0) {
    std::cout << "false" << std::endl;
  } else {
    bool ans = true;
    for(int i = 0; i < MAX; i++)
      ans &= digits[i] == 0;
    std::cout << (ans ? "true" : "false") << std::endl;
  }
  return 0;
}