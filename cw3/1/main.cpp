#include <iostream>

const char digits[] = "0123456789ABCDEF";

void printInBase(int x, int base) {
  int x_cp = x, pow = 1;
  while (x_cp >= base) {
    pow *= base;
    x_cp /= base;
  }
  while (pow > 0) {
    std::cout << digits[(x / pow) % base];
    pow /= base;
  }
}

int main() {
  int x, base;
  std::cin >> x >> base;
  printInBase(x, base);
  std::cout << std::endl;
  return 0;
}
