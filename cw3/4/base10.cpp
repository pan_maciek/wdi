#include <iostream>

const int MAX = 10000;
unsigned long long int memory[MAX];
int mem_size = 1;

void multiply(int n) {
  int carry = 0;
  for (int i = 0; i < mem_size; i++) {
    memory[i] = memory[i] * n + carry;
    carry = memory[i] / 10;
    memory[i] %= 10;
  }
  while (carry > 0) {
    memory[mem_size] = carry % 10;
    carry /= 10;
    mem_size++;
  }
}

int main() {
  int n;
  std::cin >> n;
  memory[0] = 1;
  for (int i = 2; i <= n; i++) {
    multiply(i);
  }
  std::cout << memory[mem_size - 1];
  for (int i = mem_size - 2; i >= 0; i--) {
    std::cout << memory[i];
  }
  std::cout << std::endl;
  return 0;
}