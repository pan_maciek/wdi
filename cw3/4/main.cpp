#include <iostream>

const int MAX10POW = 16; // = log10(ULLONG_MAX) - log10(1000)
const unsigned long long int MAX_VAL = 10000000000000000; // = pow(10, MAX10POW)
const int MAX = 1000;

unsigned long long int memory[MAX] = { 1 };
int mem_size = 1;

int log10(unsigned long long int x) {
  int r = 0;
  while (x > 0) {
    r++;
    x /= 10;
  }
  return r - 1;
}

void multiply(int n) {
  int carry = 0;
  for (int i = 0; i < mem_size; i++) {
    memory[i] = memory[i] * n + carry;
    carry = memory[i] / MAX_VAL;
    memory[i] %= MAX_VAL;
  }
  if (carry > 0) {
    memory[mem_size] = carry;
    mem_size++;
  }
}

int main() {
  int n;
  std::cin >> n;
  for (int i = 2; i <= n; i++) {
    multiply(i);
  }
  std::cout << memory[mem_size - 1];
  for (int i = mem_size - 2; i >= 0; i--) {
    for (int z = MAX10POW - log10(memory[i]); z > 1; z--)
      std::cout << '0';
    if (memory[i] != 0) std::cout << memory[i];
  }
  std::cout << std::endl;
  return 0;
}