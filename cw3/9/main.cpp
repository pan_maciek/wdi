#include <iostream>

const int N = 13;
const int t[N] = {2, 2, 6, 4, 10, 1, 2, 1, 1, 6, 5, 1, 1};

bool posible(int start) {
  if (start == N - 1) return true;
  if (start >= N) return false;
  int cp = t[start], i = 2;
  while (cp > 1) {
    if (cp % i == 0 && posible(start + i)) return true;
    while (cp % i == 0)
      cp /= i;
    i++;
  }
  return false;
}

int main() {
  std::cout << posible(0) << std::endl;
  return 0;
}