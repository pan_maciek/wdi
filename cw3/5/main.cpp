#include <iostream>

const int MAX_ARRAY_SIZE = 1000;
const unsigned long long int MAX_VAL = 10000000000;

struct BigUInt {
  int size;
  unsigned long long int memory[MAX_ARRAY_SIZE];
  BigUInt(unsigned long long int initial_value) {
    size = 1;
    memory[0] = initial_value;
  }
};

void mul(BigUInt &a, int n, int carry = 0) {
  for (int i = 0; i < a.size; i++) {
    a.memory[i] = a.memory[i] * n + carry;
    carry = a.memory[i] / MAX_VAL;
    a.memory[i] %= MAX_VAL;
  }
  if (carry > 0) {
    a.memory[a.size] = carry;
    a.size++;
  }
}

int cmp(const BigUInt &a, const BigUInt &b) {
  if (a.size > b.size) return 1;
  if (a.size < b.size) return -1;
  for (int i = a.size - 1; i >= 0; i--) {
    if (a.memory[i] > b.memory[i])
      return 1;
    else if (a.memory[i] < b.memory[i])
      return -1;
  }
  return 0;
}

int div(BigUInt &a, const BigUInt &b) {
  int i, value = 0, take = 0;
  while (cmp(a, b) >= 0) {
    for (i = 0; i < b.size; i++) {
      if (a.memory[i] - take >= b.memory[i]) {
        a.memory[i] -= b.memory[i] + take;
        take = 0;
      } else {
        a.memory[i] = MAX_VAL + a.memory[i] - b.memory[i] - take;
        take = 1;
      }
    }
    if (take == 1) {
      while (a.memory[i] == 0) {
        a.memory[i] = MAX_VAL - 1;
        i++;
      }
      a.memory[i] -= 1;
    }
    if (a.memory[a.size - 1] == 0) a.size--;
    value++;
  }
  return value;
}

int main() {
  BigUInt a(0), b(1);
  int precision = 100;
  std::cout << "2.";

  for (int i = 2; i < precision; i++) {
    mul(a, i, 1);
    mul(b, i, 0);
  }
  for (int i = 0; i < precision; i++) {
    mul(a, 10, 0);
    int d = div(a, b);
    std::cout << d;
  }
  std::cout << std::endl;
  return 0;
}