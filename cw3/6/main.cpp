#include <iostream>

const int MAX = 10;

int main() {
  int values[MAX] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  int x = 1, i;
  while (x != 0) {
    std::cin >> x;
    if (values[0] >= x) continue;
    for (i = 0; x > values[i] && i < MAX; i++)
      values[i] = values[i + 1];
    values[i - 1] = x;
  }
  std::cout << values[0] << std::endl;
  return 0;
}