#include <iostream>

const int N = 16;
const int t[N] = {1, 2, 3, 1, 13, 14, 15, 1, 2, 3, 4, 5, 6, 7, 8, 9};

int main() {
  int max_len = 1;
  for (int i = 1, j; i < N; i++) {
    for (j = i; j < N && t[j] - t[j - 1] > 0;) j++;
    if (j - i + 1 > max_len) max_len = j - i + 1;
    i = j;
  }
  std::cout << max_len << std::endl;
  return 0;
}