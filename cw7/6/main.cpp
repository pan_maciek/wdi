#include <iostream>

const int MAX = 6;

bool is_prime(int n) {
	if (n == 2) return true;
	if (n % 2 == 0) return false;
	if (n < 2) return false;
	for (int i = 3; i * i <= n; i += 2) {
		if (n % i == 0) return false;
	}
	return true;
}

int min(int a, int b) {
	return a > b ? b : a;
}

bool can_take_prime(int T[MAX], int i) {
	if (i == MAX) return true;
	if (T[i] == 0) return false;
	for (int val = 0, last = min(MAX, i + 30); i < last; i++) {
		val = val << 1 | T[i];
		if (is_prime(val) && can_take_prime(T, i + 1)) 
			return true;
	}
	return false;
}

int main() {
	int T[MAX] = { 1, 1, 0, 1, 0, 0 };
		
	std::cout << (can_take_prime(T, 0) ? "true" : "false") << std::endl;
}