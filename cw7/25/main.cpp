#include <cmath>
#include <iostream>

const int N = 10;

int count_jumps_to_end(int t[N], int i, int jumps) {
  if (i == N - 1) return jumps;
  if (i > N - 1) return -1; // wyskoczyliśmy poza nasz cel
  int tmp;
  // 2 jest czynnikiem pierwsyzm, liczba większa od 2, da się doskoczyć do końca
  // napisane osobno dla optymalizacji pętli poniżej, krok 2 a nie 1, wsztstkie liczby pierwsze większe od 2 sa nieparzyste
  if (t[i] % 2 == 0 && t[i] > 2 && (tmp = count_jumps_to_end(t, i + 2, jumps + 1)) != -1) return tmp;
  for (int k = 3; k * k <= t[i]; k += 2) // czynniki pierwsze muszą być mniejsz lub równe pierwiastkowi z t[i]
    if (t[i] % k == 0 && (tmp = count_jumps_to_end(t, i + k, jumps + 1)) != -1) return tmp;
  return -1; // nie udało się znaleść drogi
}

int count_jumps_to_end(int t[N]) {
  return count_jumps_to_end(t, 0, 0);
}

int main() {
  int T[N] = {9, 9, 26, 6, 25, 6, 7, 8, 9, 10};
  std::cout << count_jumps_to_end(T) << std::endl;
  return 0;
}