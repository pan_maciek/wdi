#include <iostream>

struct Position {
  int row, col;
};

// free rows, free cols, free diagonals
bool addHetmans(Position T[8], bool rows[8], bool cols[8], bool diag1[15], bool diag2[15], int i) {
  if (i == 8) return true;

  for (int row = 0, d1, d2; row < 8; row++) { // szukam pustego rzędu
    if (rows[row]) {
      rows[row] = false;
      for (int col = 0; col < 8; col++) { // szukam pustej kolumny
        d1 = row + col;                   // obliczam przekątne
        d2 = 7 - row + col;
        if (cols[col] && diag1[d1] && diag2[d2]) {
          cols[col] = diag1[d1] = diag2[d2] = false;
          if (addHetmans(T, rows, cols, diag1, diag2, i + 1)) { // szukam miejsca dla kolejnego hetmana
            T[i].row = row;
            T[i].col = col;
            return true;
          }
          // cofam aktualny krok jeśli nie uda się znaleść rozwiązania
          cols[col] = diag1[d1] = diag2[d2] = true;
        }
      }
      rows[row] = true;
    }
  }
  return false;
}

Position *solve() {
  Position *T = new Position[8];
  bool _rows[8], _cols[8], diag1[15], diag2[15];
  for (int i = 0; i < 8; i++)
    _rows[i] = _cols[i] = true;
  for (int i = 0; i < 15; i++)
    diag1[i] = diag2[i] = true;
  addHetmans(T, _rows, _cols, diag1, diag2, 0);
  return T;
}

int main() {

  Position *solution = solve();
  for (int i = 0; i < 8; i++) {
    std::cout << (char)('A' + solution[i].col) << solution[i].row + 1 << " ";
  }
}