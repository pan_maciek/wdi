#include <iostream>

int T[8][8] = {
  {0, 1, 2, 0, 0, 0, 0, 0},
  {1, 1, 1, 3, 0, 0, 0, 0},
  {0, 0, 0, 1, 4, 0, 0, 0},
  {0, 0, 0, 0, 1, 5, 0, 0},
  {0, 0, 0, 0, 0, 1, 6, 0},
  {0, 0, 0, 0, 0, 0, 7, 1},
  {0, 0, 0, 0, 0, 0, 1, 8},
  {0, 0, 0, 0, 0, 0, 0, 9},
};

int firstDigit(int n) {
  while (n > 9)
    n /= 10;
  return n;
}

bool solve(int r, int c, int prev_last) {             // prev_last = last digit of prev location
  if (r > 7 || c > 7) return false;                   // boundries check
  if (prev_last >= firstDigit(T[r][c])) return false; // first condition
  if (r == 7 && c == 7) return true;

  int last = T[r][c] % 10;
  return solve(r + 1, c, last) ||
         solve(r, c + 1, last) ||
         solve(r + 1, c + 1, last);
}

bool solve(int r, int c) {
  return solve(r, c, -1);
}

int main() {
  int r = 0, c = 0;
  std::cout << (solve(r, c) ? "ture" : "false") << std::endl;
}