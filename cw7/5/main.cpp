#include <iostream>
const int N = 5;

bool can_move(int sol[N][N], int y, int x) {
	if (y < 0 || y >= N) return false;
	if (x < 0 || x >= N) return false;
	return sol[y][x] == 0;
}

const int offset_x[8] = { 2, 1, -1, -2, -2, -1,  1,  2 };
const int offset_y[8] = { 1, 2,  2,  1, -1, -2, -2, -1 };

int move(int sol[N][N], int y, int x, int i) {
	sol[y][x] = i;
	if (i == N * N) return true;
	for (int k = 0, next_y, next_x; k < 8; k++) {
		next_y = y + offset_y[k];
		next_x = x + offset_x[k];
		if (can_move(sol, next_y, next_x) && move(sol, next_y, next_x, i + 1)) 
			return true;
	}
	sol[y][x] = 0;
	return false;
}


void printSolution(int sol[N][N]) {
	for (int x = 0; x < N; x++) {
		for (int y = 0; y < N; y++)
			std::cout << sol[x][y] << '\t';
		std::cout << std::endl;
	}
}

bool solveKT(int y, int x) {
	int sol[N][N];

	for (int x = 0; x < N; x++)
		for (int y = 0; y < N; y++)
			sol[x][y] = 0;
	
	if (!move(sol, y, x, 1)) {
		std::cout << "Solution does not exist" << std::endl;
		return false;
	} else printSolution(sol);
	return true;
}

int main() {
	solveKT(0,0);
	return 0;
}