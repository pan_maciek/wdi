#include <iostream>

int isPrime(int n) {
  if (n == 2) return true;
  if (n < 2) return false;
  if (n % 2 == 0) return false;

  for (int i = 3; i * i <= n; i += 2)
    if (n % i == 0) return false;

  return true;
}

int solve(int A, int B, int val) {
  if (A < 0 || B < 0) return 0;
  if (A == 0 && B == 0) return isPrime(val) ? 0 : 1;
  return solve(A - 1, B, val << 1 | 1) + solve(A, B - 1, val << 1);
}

int solve(int A, int B) {
  return solve(A - 1, B, 1);
}

int main() {
  std::cout << solve(10, 20) << std::endl;
  return 0;
}
