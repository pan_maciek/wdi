#include <iostream>

const int MAX = 6;

struct Result { int len; int sum; bool ok; };

Result sub(int T[MAX], int i, int sum, int index_sum, int len) {
	if (i == MAX) {
		Result r;
		r.ok = index_sum == sum;
		r.len = len;
		r.sum = sum;
		return r;
	}

	Result r1 = sub(T, i + 1, sum + T[i], index_sum + i, len + 1);
	Result r2 = sub(T, i + 1, sum, index_sum, len);
	if (r1.ok && !r2.ok) return r1;
	if (!r1.ok && r2.ok) return r2;
	return r1.len > r2.len ? r1 : r2;
}

int find_longest_subbaray(int T[MAX]) {
	Result x = sub(T, 0, 0, 0, 0);
	return x.sum;
}

int main() {
	int T[MAX] = { 1, 7, 3, 5, 11, 2 };

	std::cout << find_longest_subbaray(T) << std::endl;
	return 0;
}