#include <iostream>
const int N = 5;
int t[] = {2, 3, 5, 7, 15};

int count_1s_in_bin(int n) {
  int count = 0;
  while (n > 0) {
    if (n & 1 == 1) count++; // & można zastąpić % 2 ale no and bitowy jest fajny xd
    n = n >> 1; // przesunięcie bitowe można zastąpić dzieleniem przez 2 ale tak wygląda profesjonalnie :P
  }
  return count;
}

// zakładamy że 0 nie jest naturalne
bool can_split(int t[N], int i, int A, int B, int C) {

  if (A != 0 && A == B && B == C) return true;
  if (i == N) return false;

  int _1s = count_1s_in_bin(t[i]);
  return can_split(t, i + 1, A, B, C) || // jeśli trzeba wykorzystać wszystkie elementy nie powinno być tej lini
         can_split(t, i + 1, A + _1s, B, C) ||
         can_split(t, i + 1, A, B + _1s, C) ||
         can_split(t, i + 1, A, B, C + _1s);
}

bool can_split(int t[N]) {
  return can_split(t, 0, 0, 0, 0);
}

int main() {
  std::cout << (can_split(t) ? "true" : "false") << std::endl;
  return 0;
}
