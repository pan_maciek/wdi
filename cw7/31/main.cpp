#include <iostream>
const int N = 10;

void Nka(int t[N], int i, int n, const int target_n, int sum, const int target_sum, int &count) {
	if (n > target_n) return;
	if (n == target_n && sum == target_sum) {
		count++;
		return;
	}
	if (i - n > N - target_n) return;

	Nka(t, i + 1, n + 1, target_n, sum + t[i], target_sum, count);
	Nka(t, i + 1, n, target_n, sum, target_sum, count);
}

int Nka(int t[N], int target_n, int target_sum) {
	int count = 0;
	Nka(t, 0, 0, target_n, 0, target_sum, count);
	return count;
}

int main() {
	int t[N] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 8 };
	std::cout << Nka(t, 2, 16) << std::endl;
	return 0;
}