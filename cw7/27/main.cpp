#include <iostream>
const int N = 20;
struct kwadrat {
  int x1, x2, y1, y2;
};
kwadrat t[N];

bool in_range(int min, int max, int val) {
  return val >= min && val <= max;
}

bool intersects(int a_min, int a_max, int b_min, int b_max) {
  return in_range(a_min, a_max, b_min) || in_range(a_min, a_max, b_max);
}

bool intersects(kwadrat a, kwadrat b) {
  return intersects(a.x1, a.x2, b.x1, b.x2) && intersects(a.y1, a.y2, b.y1, b.y2);
}

int field(kwadrat a) {
  return (a.x2 - a.x1) * (a.y2 - a.y1);
}

bool can_find(kwadrat t[N], int i, kwadrat s[13], int sel, int sum) {
  if (sel == 13) return sum == 2012;
  if (i == N) return false;
  for (int j = 0; j < sel; j++) {
    if (intersects(t[i], s[j])) return false;
  }
  s[sel] = t[i];
  return can_find(t, i + 1, s, sel + 1, sum + field(t[i])) || can_find(t, i + 1, s, sel, sum);
}

int main() {
  for (int i = 0; i < N; i++) {
    t[i].x1 = i;
    t[i].x2 = i + 5;
    t[i].y1 = i;
    t[i].y2 = i + 5;
  }
  t[0].x1 = 1;
  t[0].x2 = 10;
  t[0].y1 = 0;
  t[0].y2 = 10;
  kwadrat y[13];
  std::cout << can_find(t, 0, y, 0, 0);
}