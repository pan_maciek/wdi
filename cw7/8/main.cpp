#include <iostream>

int min(int a, int b) {
  return a > b ? b : a;
}
bool can_move(int y, int x) {
  if (x < 0 || x > 7) return false;
  if (y < 0 || y > 7) return false;
  return true;
}
int min_price(int y, int x, int T[8][8]) {
  if (y == 8) return 0;
  if (!can_move(y, x)) return INT_MAX;
  int a = min_price(y + 1, x, T);
  int b = min_price(y + 1, x - 1, T);
  int c = min_price(y + 1, x + 1, T);
  return T[y][x] + min(a, min(b, c));
}

int main() {

  int T[8][8] = {
      {1, 1, 1, 1, 1, 1, 1, 1},
      {2, 2, 1, 1, 1, 1, 1, 1},
      {1, 2, 2, 1, 5, 1, 1, 1},
      {1, 1, 2, 2, 5, 1, 1, 1},
      {1, 1, 1, 2, 5, 1, 1, 1},
      {3, 3, 3, 5, 11, 1, 1, 1},
      {5, 5, 5, 5, 1, 1, 1, 1},
      {1, 1, 1, 1, 10, 10, 10, 10}};
  std::cout << min_price(0, 0, T) << std::endl;
  return 0;
}