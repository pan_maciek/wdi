#include <iostream>

// "wycina" z n wielokrotność w
int cut(int n, int w) {
  int back = n % w;
  int front = (n / w) / 10;
  return front * w + back;
}

bool is_prime(int n) {
  if (n < 2) return false;
  if (n == 2) return true;
  if (n % 2 == 0) return false;
  for (int i = 3; i * i <= n; i += 2)
    if (n % i == 0) return false;
  return true;
}

// r - ilość usuniętych elementów
// w - określa który element będziemy usuwać
void print_primes(int n, int w, int r) {
  if (n < 10) return; // conajmniej 2 cyfrowe
  if (r > 0 && is_prime(n)) std::cout << n << ' '; // liczby pierwsze, powstałe poprzez wykreślenie (r == 0 nic nie było wykreślone) 
  if (w == 0) return; // nie wykreślilibyśmy już żadnej liczby
  print_primes(cut(n, w), w / 10, r + 1);
  print_primes(n, w / 10, r);
}

void print_primes(int n) {
  int n_cp = n, w = 1;
  while (n_cp > 9) {
    w *= 10;
    n_cp /= 10;
  }
  print_primes(n, w, 0);
  std::cout << std::endl;
}

int main() {
  print_primes(12345);
  return 0;
}
