#include <iostream>

const int MAX = 20;

int is_prime(int n) {
  if (n == 2) return true;
  if (n < 2) return false;
  if (n % 2 == 0) return false;

  for (int i = 3; i * i <= n; i += 2)
    if (n % i == 0) return false;

  return true;
}

int fill_divs(int n, int T[MAX]) { // wypełnia tablicę podzielnikami pierwszymi liczby n i zwraca "długość"
  int i = 0, div = 2;
  while (n > 1) {
    if (n % div == 0) {
      if (is_prime(div)) T[i++] = div;
      do {
        n /= div;
      } while (n % div == 0);
    }
    div++;
  }
  return i;
}

int solve(int divs[MAX], int i, int prod) {
  if (i < 0) return prod; // zwracam iloczyn podzbioru, dla podzbioru pustego będzie on wynosił 1
  return solve(divs, i - 1, prod * divs[i]) + solve(divs, i - 1, prod); // generowanie podzbiorów
}

int solve(int n) {
  int divs[20];
  int len = fill_divs(n, divs);
  return solve(divs, len - 1, 1) - 1; // -1 usuwam zbiór pusty
}

int main() {
  std::cout << solve(60) << std::endl;
}