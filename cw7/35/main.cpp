#include <iostream>
const int N = 10;

bool can_split(int t[N], int i, const int k, int A, int B, int A_len, int B_len) {
  if (A_len == k && k == B_len) return A == B;
  if (i == N) return false;

  return can_split(t, i + 1, k, A, B, A_len, B_len) ||
         can_split(t, i + 1, k, A + t[i], B, A_len + 1, B_len) ||
         can_split(t, i + 1, k, A, B + t[i], A_len, B_len + 1);
}

bool solve(int t[N], int k) {
  return can_split(t, 0, k, 0, 0, 0, 0);
}

int main() {
  int t[N] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  std::cout << (solve(t, 3) ? "true" : "false") << std::endl;
  return 0;
}