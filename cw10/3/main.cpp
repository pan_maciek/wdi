#include <iostream>

struct Node { int val; Node* next; };

int min(int a, int b) { return a > b ? b : a; }

void init(Node*& root) {
	root = NULL;
}

void set_insert(Node *&root, int val) {
	Node *p = root, *q = NULL;
	while (p != NULL && p->val < val) {
		q = p;
		p = p->next;
	}
	if (p != NULL && p->val == val) return;
	Node *tmp = new Node;
	tmp->val = val;
	tmp->next = p;
	if (q == NULL) root = tmp;
	else q->next = tmp;
}


Node* merge(Node* list1, Node* list2) {
	if (list1 == NULL && list2 == NULL) return NULL;

	Node* root = new Node, *p = root, *q = NULL;

	while (list1 != NULL && list2 != NULL) {
		p->val = min(list1->val, list2->val);
		if (list1->val == p->val) list1 = list1->next;
		if (list2->val == p->val) list2 = list2->next;
		q = p;
		p->next = new Node;
		p = p->next;
	}

	if (list2 != NULL) list1 = list2;
	while (list1 != NULL) {
		p->val = list1->val;
		list1 = list1->next;
		q = p;
		p->next = new Node;
		p = p->next;
	}
	q->next = NULL;
	delete p;
	return root;
}


int main() {

	Node *list1, *list2;
	init(list1);
	init(list2);
	set_insert(list1, 1);
	set_insert(list1, 11);
	set_insert(list1, 12);
	set_insert(list1, 14);

	set_insert(list2, 4);
	set_insert(list2, 14);
	set_insert(list2, 15);
	set_insert(list2, 16);
	merge(list1, list2);
}