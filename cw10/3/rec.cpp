#include <iostream>

struct Node { int val; Node* next; };

int min(int a, int b) { return a > b ? b : a; }

void init(Node*& root) {
	root = NULL;
}

void set_insert(Node *&root, int val) {
	Node *p = root, *q = NULL;
	while (p != NULL && p->val < val) {
		q = p;
		p = p->next;
	}
	if (p != NULL && p->val == val) return;
	Node *tmp = new Node;
	tmp->val = val;
	tmp->next = p;
	if (q == NULL) root = tmp;
	else q->next = tmp;
}


void merge(Node* l1, Node* l2, Node*& r) {
	if (l1 == NULL && l2 == NULL) r = NULL;
	else if (l1 == NULL) merge(l2, NULL, r);
	else {
		r = new Node;
		if (l2 == NULL) {
			r->val = l1->val;
			merge(l1->next, NULL, r->next);
		} else {
			r->val = min(l1->val, l2->val);
			merge(l1->val == r->val ? l1->next : l1, l2->val == r->val ? l2->next : l2, r->next);
		}
	}
}

Node* merge(Node* list1, Node* list2) {
	Node* ref;
	merge(list1, list2, ref);
	return ref;
}

void print(Node* root) {
	if (root == NULL) return;
	std::cout << root->val << " ";
	print(root->next);
}

int main() {

	Node *list1, *list2;
	init(list1);
	init(list2);
	set_insert(list1, 1);
	set_insert(list1, 11);
	set_insert(list1, 13);
	set_insert(list1, 14);

	set_insert(list2, 4);
	set_insert(list2, 12);
	set_insert(list2, 14);
	set_insert(list2, 15);
	set_insert(list2, 16);
	print(merge(list1, list2));
}