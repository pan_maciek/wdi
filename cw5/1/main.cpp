#include <iostream>

struct ulamek { int l, m; };

int nwd(int a, int b) {
	if (b == 0) return a;
	else return nwd(b, a % b);
}

void skroc(ulamek &a) {
  int tmp = nwd(a.l, a.m);
	a.l /= tmp;
	a.m /= tmp;
}

ulamek operator+(const ulamek &a, const ulamek &b) {
	int tmp = nwd(a.m, b.m);
	ulamek c;
	c.m = a.m * b.m / tmp;
	c.l = a.l * b.m / tmp + b.l * a.m / tmp;
	tmp = nwd(c.l, c.m);
	c.l /= tmp;
	c.m /= tmp;
	return c;
}

ulamek operator-(const ulamek &a, const ulamek &b) {
	int tmp = nwd(a.m, b.m);
	ulamek c;
	c.m = a.m * b.m / tmp;
	c.l = a.l * b.m / tmp - b.l * a.m / tmp;
  skroc(c);
	return c;
}

ulamek operator*(const ulamek &a, const ulamek &b) {
	ulamek c;
	c.m = a.m * b.m;
	c.l = a.l * b.l;
  skroc(c);
	return c;
}

ulamek operator/(const ulamek &a, const ulamek &b) {
	ulamek c;
	c.m = a.m * b.l;
	c.l = a.l * b.m;
  skroc(c);
	return c;
}

ulamek pow(const ulamek &a, unsigned int p) {
  if (p == 1) return a;
  ulamek u;
  u.l = u.m = 1;
  if (p == 0) {
    return u;
  }
  
  while(p > 0) { 
    u = u * a;
    p--;
  }
  return u;
}

std::ostream& operator<<(std::ostream& out, const ulamek& o) {
  out << o.l << '/' << o.m;
  return out;
}

std::istream& operator>>(std::istream &input, ulamek &o) {
  input >> o.l >> o.m;
  return input;
}



int main() {
	ulamek a, b;
	a.l = 2; a.m = 13;  
	b.l = 1; b.m = 5;
	std::cout << pow(b, 3) << std::endl;
}