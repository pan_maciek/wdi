#include <iostream>
using namespace std;

const double step = 1e-6;

double f(double x) {
  return 1 / x;
}

int main() {
  double k, i = 1 + step, sum = 0;
  cin >> k;
  while (i <= k) {
    sum += f(i) * step;
    i += step;
  }
  cout << sum << endl;
}