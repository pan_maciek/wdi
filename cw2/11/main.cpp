#include <iostream>

int main() {
  int An = 2, x;
  std::cin >> x;
  while (An < x) {
    if (x % An == 0) break;
    An = 3 * An + 1;
  }
  std::cout << (x % An == 0 ? "true" : "false") << std::endl;
  return 0;
}