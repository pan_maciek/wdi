#include <iostream>
#include <cmath>

int main() {
  int x, n;
  std::cin >> x;
  n = log10(x) + 1;
  while(x > 0) {
    if (x % 10 == n) {
      std::cout << "true" << std::endl;
      return 0;
    }
    x /= 10;
  }
  std::cout << "false" << std::endl;
  return 0;
}