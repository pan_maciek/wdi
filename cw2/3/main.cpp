#include <iostream>
using namespace std;

bool is_palindrome(int n, int base) {
  int r = 0, n_cp = n;
  while (n > 0) {
    r = r * base + n % base;
    n = n / base;
  }
  return r == n_cp;
}

int main() {
  int n;
  cin >> n;
  cout << (is_palindrome(n, 10) ? "true" : "false") << endl;
  cout << (is_palindrome(n, 2) ? "true" : "false") << endl;
  return 0;
}
