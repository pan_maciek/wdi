#include <cmath>
#include <iostream>

int reverse(int a) {
  int r = 0;
  while (a > 0) {
    r = r * 10 + a % 10;
    a /= 10;
  }
  return r;
}

bool is_prime(int x) {
  if (x < 2) return false;
  if (x == 2) return true;
  if (x % 2 == 0) return false;
  int i = 3;
  while (i * i <= x) {
    if (x % i == 0) return false;
    i += 2;
  }
  return true;
}

int count = 0;
void create_number(int a, int b, int num) {
  if (a + b == 0 && is_prime(num)) count++;
  if (a != 0) create_number(a / 10, b, num * 10 + a % 10);
  if (b != 0) create_number(a, b / 10, num * 10 + b % 10);
}

int main() {
  int a, b, mask;
  std::cin >> a >> b;
  create_number(reverse(a), reverse(b), 0);
  std::cout << count << std::endl;
  return 0;
}