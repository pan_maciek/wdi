#include <cmath>
#include <iostream>

const int MAX = 1000000;
int primes[MAX] = {2, 0, 2};

int sum_digits(int a) {
  int sum = 0;
  while (a > 0) {
    sum += a % 10;
    a /= 10;
  }
  return sum;
}

bool is_smiths_number(int a) {
  int digits_sum = sum_digits(a);
  int n = 0, tmp;
  while (a > 1) {
    tmp = primes[n];
    while (a % tmp == 0) {
      digits_sum -= sum_digits(tmp);
      a /= tmp;
    }
    n++;
  }
  return digits_sum == 0;
}

int main() {
  int last_prime_index = 0;
  for (int i = 3; i < MAX; i += 2)
    primes[i] = i;
  for (int i = 2; i < MAX; i++) {
    if (primes[i] != 0) {
      for (int j = 2 * i; j < MAX; j += i)
        primes[j] = 0;
      primes[++last_prime_index] = i;
    } else {
      if (is_smiths_number(i))
        std::cout << i << ' ';
    }
  }

  return 0;
}