#include <iostream>

int main() {
  int x, last = 0, all = 0;
  std::cin >> x;
  last = 1 << (x % 10);
  x /= 10;
  while (x > 0) {
    all |= 1 << (x % 10);
    x /= 10;
  }
  std::cout << ((all & last) == 0 ? "true" : "false") << std::endl;
  return 0;
}