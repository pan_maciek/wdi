#include <cmath>
#include <iostream>

int main() {
  int n;
  std::cin >> n;

  int digits[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  for (int i = 0; i < 10; i++)
    digits[i] = pow(i, n);

  for (int i = pow(10, n - 1), max = pow(10, n), i_cp, sum; i < max; i++) {
    i_cp = i;
    sum = 0;
    while (i_cp > 0) {
      sum += digits[i_cp % 10];
      i_cp /= 10;
    }
    if (sum == i) std::cout << i << std::endl;
  }

  return 0;
}