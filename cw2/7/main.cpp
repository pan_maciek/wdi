#include <cmath>
#include <iostream>

int main() {
  int x;
  std::cin >> x;
  int a = sqrt(x);
  while (x % a != 0) a--;
  std::cout << a << "*" << x / a << std::endl;
  return 0;
}