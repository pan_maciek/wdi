#include <iostream>

int main() {
  int a, b, n;
  std::cin >> a >> b >> n;
  while (n > 0) {
    a *= 10;
    std::cout << a / b;
    a %= b;
    n--;
  }
  std::cout << std::endl;
  return 0;
}