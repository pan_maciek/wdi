#include <iostream>
using namespace std;

int pow2digits[] = {2, 4, 8, 6};
int last_2pow_digit(int pow) {
  return pow2digits[(pow - 1) % 4];
}

int fact_digits[] = {1, 1, 2, 6, 4};
int last_nonzero_fact_digit(int n) {
  if (n < 5) return fact_digits[n];
  int A = n / 5, B = n % 5;
  return (last_2pow_digit(A) * last_nonzero_fact_digit(A) * fact_digits[B]) % 10;
}

int main() {
  int n;
  cin >> n;
  cout << last_nonzero_fact_digit(n) << endl;
  return 0;
}
