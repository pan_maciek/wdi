#include <cmath>
#include <iostream>

int NWD(int a, int b) {
  if (b == 0) return a;
  return NWD(b, a % b);
}

int main() {
  int a, b, p2 = 0, p5 = 0, tmp, nwd;
  std::cin >> a >> b;

  // skracamy ułamki do jak najprostszej formy
  nwd = NWD(a, b);
  a /= nwd;
  b /= nwd;

  std::cout << a / b << '.';

  // zajmujemy się ułamkami z przedziału (0, 1)
  a = a % b;
  tmp = b;
  // liczymy wystąpienia 2 i 5 w mianowniku
  while (tmp % 2 == 0) {
    tmp /= 2;
    p2++;
  }
  tmp = b;
  while (tmp % 5 == 0) {
    tmp /= 5;
    p5++;
  }

  // na tej zasadzie określamy ile miejsc po przecinku będziemy musieli zapisać zanim trafimy na rozwinięcie okresowe
  int n = std::max(p2, p5);
  while (n > 0) {
    a *= 10;
    std::cout << a / b;
    a %= b;
    n--;
  }

  int r = a;
  if (r != 0) {
    std::cout << '(';
    // dopuki reszta z dzielenia nie powtórzy sie z pierwszą resztą wypisujemy kolejne miejsca ułamku okresowego
    do {
      a *= 10;
      std::cout << a / b;
      a %= b;
    } while (a % b != r);
    std::cout << ')';
  }
  std::cout << std::endl;
  return 0;
}
