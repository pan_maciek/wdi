#include <iostream>

int is_in_fib(int n) {
  int a = 1, b = 1, c;
  while (a < n) {
    c = b;
    b = a;
    a = b + c;
  }
  return a == n;
}

int main() {
  int n;
  std::cin >> n;
  int a = 1, b = 1, c;
  while (a <= n) {
    if (n % a == 0 && is_in_fib(n / a)) {
      std::cout << "true" << std::endl;
      return 0;
    }
    c = b;
    b = a;
    a = b + c;
  }
  std::cout << "false" << std::endl;
  return 0;
}