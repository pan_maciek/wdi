#include <iostream>

bool can_be_rep_by_sum_fib(int x, int sum) {
  int a = 1, b = 0, c;
  a = 1;
  b = 0;
  while (sum > x) {
    sum -= a;
    c = b;
    b = a;
    a = c + b;
  }
  return sum == x;
}

int main() {
  int x, a = 1, b = 0, c, sum = 0;
  std::cin >> x;
  do {
    x++;
    while (sum < x) {
      sum += a;
      c = b;
      b = a;
      a = c + b;
    }
  } while (can_be_rep_by_sum_fib(x, sum));
  std::cout << x << std::endl;
  return 0;
}