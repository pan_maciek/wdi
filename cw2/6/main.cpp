#include <cmath>
#include <iostream>

int create_number(int base_number, int mask) {
  int n = 0, pow = 1;
  while (mask != 0) {
    if (mask & 1 == 1) {
      n += (base_number % 10) * pow;
      pow *= 10;
    }
    base_number /= 10;
    mask >>= 1;
  }
  return n;
}

int main() {
  int x;
  std::cin >> x;

  int n = log10(x) + 1;

  int count = 0, tmp;
  int mask = (1 << n) - 1;
  while (mask > 0) {
    if (create_number(x, mask) % 7 == 0) count++;
    mask--;
  }
  std::cout << count << std::endl;
  return 0;
}