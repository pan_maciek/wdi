#include <iostream>

int A(int n) {
  return n * n + n + 1;
}

int main(int argc, char const *argv[]) {
  int x, n = 1, tmp;
  std::cin >> x;
  while ((tmp = A(n)) <= x) {
    if (x % tmp == 0) {
      std::cout << "true" << std::endl;
      return 0;
    }
    n++;
  }
  std::cout << "false" << std::endl;
  return 0;
}
