#include <iostream>

int main() {
  int n, count = 0;
  std::cin >> n;
  for (int p2 = 1; p2 <= n; p2 *= 2)
    for (int p3 = p2; p3 <= n; p3 *= 3)
      for (int p5 = p3; p5 <= n; p5 *= 5)
        count++;
  std::cout << count << std::endl;
  return 0;
}