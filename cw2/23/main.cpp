#include <iostream>

unsigned int compress_digits(int n, int base) {
  unsigned int d = 0;
  while (n > 0) {
    d |= 1 << (n % base);
    n /= base;
  }
  return d;
}

bool has_unique_digits(int a, int b, int base) {
  return (compress_digits(a, base) & compress_digits(b, base)) == 0;
}

int main() {
  int a, b;
  std::cin >> a >> b;
  for (int base = 2; base <= 16; base++) {
    if (has_unique_digits(a, b, base)) {
      std::cout << base << std::endl;
      return 0;
    }
  }
  std::cout << "lack of an appropriate numerical basis" << std::endl;
  return 0;
}