#include <iostream>

int main() {
  int x, prev = 10;
  std::cin >> x;

  while(x > 0) {
    if (prev <= x % 10) break;
    prev = x % 10;
    x /= 10;
  }
  std::cout << (x == 0 ? "true" : "false") << std::endl;
  return 0;
}